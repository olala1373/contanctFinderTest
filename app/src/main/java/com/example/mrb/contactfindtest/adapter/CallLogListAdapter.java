package com.example.mrb.contactfindtest.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrb.contactfindtest.Permissions;
import com.example.mrb.contactfindtest.R;
import com.example.mrb.contactfindtest.baseClasses.CallLogBase;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallLogListAdapter extends RecyclerView.Adapter<CallLogListAdapter.ViewHolder>{

    private ArrayList<CallLogBase> callLogs;
    private int layout;
    private int count = 0;
    private Activity context;
    ///////////////////////////////////
    private final int BLUE = R.color.blueA700;
    private final int PURPLE100 = R.color.purpleA700;
    private final int REDA700 = R.color.redA700;
    private final int ORANGE = R.color.orangeA700;
    private final int CYAN100 = R.color.cyanA700;
    private final int GREENA700 = R.color.greenA700;



    public CallLogListAdapter(ArrayList<CallLogBase> callLogs , int layout , Activity context){
        this.callLogs = callLogs;
        this.layout = layout;
        this.context = context;

    }

    public void setData(ArrayList<CallLogBase> callLogs){
        this.callLogs = callLogs;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public CallLogListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout , parent , false);
        return new CallLogListAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull CallLogListAdapter.ViewHolder holder, int position) {
        Log.v("onBindViewHolder" , "size = " + callLogs.size());
        CallLogBase callLog = callLogs.get(position);
        holder.name.setText(callLog.getName());
        int pos = position % 5 ;
        switch (pos){
            case 0:
                holder.circleImageView.setFillColor(context.getResources().getColor(REDA700));
                break;
            case 1:
                holder.circleImageView.setFillColor(context.getResources().getColor(GREENA700));
                break;
            case 2:
                holder.circleImageView.setFillColor(context.getResources().getColor(PURPLE100));
                break;
            case 3:
                holder.circleImageView.setFillColor(context.getResources().getColor(ORANGE));
                break;
            case 4:
                holder.circleImageView.setFillColor(context.getResources().getColor(BLUE));
                break;
        }

        holder.calldetail.removeAllViews();
        for (int type : callLogs.get(position).getType()){
            ImageView view = new ImageView(context);
            switch (type){
                case CallLog.Calls.OUTGOING_TYPE:
                    view.setImageResource(R.drawable.outgoing1);
                    holder.calldetail.addView(view);
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    view.setImageResource(R.drawable.incomming1);
                    holder.calldetail.addView(view);
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    view.setImageResource(R.drawable.missed1);
                    holder.calldetail.addView(view);
                    break;
            }


        }

        final String phone = callLogs.get(position).getNumber();

        final Permissions permissions = new Permissions(context);


        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phone));

                if (!permissions.checkPermission(Manifest.permission.CALL_PHONE)){
                    permissions.requestPermission(Manifest.permission.CALL_PHONE , 3);
                }
                else if (permissions.checkPermission(Manifest.permission.CALL_PHONE))
                    context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return callLogs.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public CircleImageView circleImageView;
        public LinearLayout calldetail;
        public ImageButton call;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.call_contact_name);
            circleImageView = itemView.findViewById(R.id.circleImageView);
            calldetail = itemView.findViewById(R.id.call_detail);
            call = itemView.findViewById(R.id.call_button);

        }
    }
}
