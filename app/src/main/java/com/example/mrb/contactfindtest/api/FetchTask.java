package com.example.mrb.contactfindtest.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.security.PrivateKey;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by mrb on 4/8/18.
 */

public class FetchTask extends AsyncTask<Void , Void , JSONObject> {

    private IRequestCallBack requestCallBack;
    private Request request;
    private InputStream inputStream;
    private String responseString;
    private String message;
    private Context context;
    private ProgressDialog progressDialog;
    public FetchTask(Request request){
        this.request = request;
        requestCallBack = request.getCallBack();
    }

    public FetchTask(Request request , Context context , String message){
        this.request = request;
        this.context = context;
        this.message = message;
        requestCallBack = request.getCallBack();
    }

    @Override
    protected void onPreExecute() {

        if (context != null){
             progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
        if (requestCallBack != null){
            NetworkInfo networkInfo = requestCallBack.getActiveNetworkInfo();
            if (networkInfo ==null || !networkInfo.isConnected() || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                    && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)){
                Log.v("NETWORK" , "problem");
                cancel(true);
            }
        }
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        try {
            request.generateConnection();
            request.start();
            int responseCode = request.getResponseCode();
            if (responseCode != HttpsURLConnection.HTTP_OK){
                requestCallBack.onError(responseCode);
                throw new IOException("HTTP Error Code :" + responseCode);
            }

            inputStream = request.getStream();
            if (inputStream != null){
                responseString = readStream(inputStream , 100000);
                return new JSONObject(responseString);
            }
        }
        catch (IOException | JSONException e){
            e.printStackTrace();
            return null;
        }
        finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
            request.disconnect();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        if (context!=null){
            if (progressDialog.isShowing()){
                progressDialog.setMessage("Done");
                progressDialog.dismiss();
            }
        }
        requestCallBack.onFinish(jsonObject);
    }

    @Override
    protected void onCancelled() {
        requestCallBack.onCancel(true);
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        requestCallBack.onCancel(true);
    }

    public String readStream(InputStream inputStream , int maxReadSize) throws IOException , UnsupportedEncodingException{
        Reader reader = null;
        reader = new InputStreamReader(inputStream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;
        StringBuffer buffer = new StringBuffer();
        while (((readSize = reader.read(rawBuffer)) != -1) && maxReadSize > 0) {
            if (readSize > maxReadSize) {
                readSize = maxReadSize;
            }
            buffer.append(rawBuffer, 0, readSize);
            maxReadSize -= readSize;
        }
        return buffer.toString();

    }
}
