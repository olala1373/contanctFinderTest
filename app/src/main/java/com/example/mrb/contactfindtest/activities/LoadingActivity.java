package com.example.mrb.contactfindtest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.mrb.contactfindtest.MainActivity;
import com.example.mrb.contactfindtest.R;

import butterknife.BindView;

public class LoadingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        final ImageView image = (ImageView) findViewById(R.id.app_icon);
        final RotateAnimation rotateAnimation = new RotateAnimation(30 , 90 , Animation.RELATIVE_TO_SELF ,
                0.5f , Animation.RELATIVE_TO_SELF , 0.5f);
        rotateAnimation.setDuration(500);
        rotateAnimation.setRepeatCount(1);
        image.setAnimation(rotateAnimation);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                    Intent intent = new Intent(getApplicationContext() , MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {

                }

            }
        }).start();





    }


}
