package com.example.mrb.contactfindtest.api;

import android.content.Context;
import android.media.session.MediaSession;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by mrb on 4/8/18.
 */

public class Request {

    private String Url;
    private String token;
    private LanguageType languageType;
    private MethodType methodType;
    private JSONObject body;
    private long timeout , defaultTimeout = 7000;
    private boolean ssl = false;
    private IRequestCallBack callBack;
    private HttpURLConnection insecureConnection;
    private HttpsURLConnection secureConnection;
    private FetchTask fetchTask;

    public enum MethodType{
        GET ,
        POST
    }

    public enum LanguageType{
        FA ,
        EN
    }

    private Request(String url , String token , LanguageType languageType , MethodType method , long timeOut ,
                    boolean ssl){
        this.Url = url;
        this.token = token;
        this.languageType = languageType;
        this.methodType = method;
        this.timeout = timeOut;
        this.ssl = ssl;
    }

    private void setBody(JSONObject body){
        this.body = body;
    }

    public void generateConnection() throws IOException{
        if (ssl){
            this.secureConnection = getSecureConnection();
        }
        else {
            this.insecureConnection = getInsecureConnection();
        }
    }

    public void fetch(){
        fetchTask = new FetchTask(this);
        fetchTask.execute();
    }

    public void fetchLoad(Context context , String message){
        fetchTask = new FetchTask(this , context , message);
        fetchTask.execute();
    }

    public void start() throws IOException{
        if (ssl){
            secureConnection.connect();
        }
        else {
            insecureConnection.connect();
        }
    }

    public int getResponseCode() throws IOException{
        if (ssl){
            return secureConnection.getResponseCode();
        }
        else {
            return insecureConnection.getResponseCode();
        }
    }

    public InputStream getStream() throws IOException{
        if (ssl){
            return secureConnection.getInputStream();
        }
        else {
            return insecureConnection.getInputStream();
        }
    }

    public void disconnect(){
        if (ssl){
            secureConnection.disconnect();
        }
        else {
            try{
                insecureConnection.disconnect();
            }
            catch (NullPointerException e){
                Log.v("DISCONNECT" , "URL CONNECTION IS NOT REACHABLE");
            }

        }
    }

    public IRequestCallBack getCallBack(){
        return this.callBack;
    }

    private HttpURLConnection getInsecureConnection() throws IOException {
        URL requestURL = new URL(this.Url);
        HttpURLConnection urlConnection = (HttpURLConnection) requestURL.openConnection();
        attachHeaders(urlConnection);
        attachMethod(urlConnection);
        urlConnection.setConnectTimeout((int) ((int) this.timeout != 0 ? this.timeout : this.defaultTimeout));
        urlConnection.setReadTimeout((int) ((int) this.timeout != 0 ? this.timeout : this.defaultTimeout));
        attachBody(urlConnection);
        return urlConnection;
    }

    private void attachMethod(HttpURLConnection connection) throws ProtocolException {
        switch (methodType){
            case GET:
                connection.setRequestMethod("GET");
                break;
            case POST:
                connection.setRequestMethod("POST");
                break;
        }
    }


    private HttpsURLConnection getSecureConnection() throws IOException{
        URL requestURL = new URL(this.Url);
        HttpsURLConnection urlConnection = (HttpsURLConnection) requestURL.openConnection();
        attachHeaders(urlConnection);
        attachMethod(urlConnection);
        urlConnection.setConnectTimeout((int) this.timeout);
        urlConnection.setReadTimeout((int) this.timeout);
        attachBody(urlConnection);
        return urlConnection;
    }

    private void attachHeaders(HttpURLConnection connection) {
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept-Language", getLanguage());
        if(token != null){
            connection.setRequestProperty("Authorization", "Bearer " + token);
        }
    }

    private String getLanguage(){
        switch (languageType){
            case EN:
                return "en";
            case FA:
                return "fa";
            default:
                return "fa";
        }
    }
    private void attachBody(HttpURLConnection connection) throws IOException {
        if(methodType != MethodType.GET && body != null){
            connection.setDoInput(true);
            connection.setDoOutput(true);
            OutputStream os = connection.getOutputStream();
            if (os != null){
                os.write(body.toString().getBytes("UTF-8"));
                os.close();
            }

        }
    }

    public void setCallBack(IRequestCallBack callBack){
        this.callBack = callBack;
    }


    public static class Builder{
        private String Url;
        private String token;
        private LanguageType languageType;
        private MethodType methodType;
        private long timeout;
        private IRequestCallBack callBack;
        private JSONObject body;
        private boolean ssl;


        public Builder(){}

        public Builder setUrl(String Url){
            this.Url = Url;
            return this;
        }

        public Builder setLanguage(LanguageType language){
            this.languageType = language;
            return this;
        }

        public Builder setToken(String token){
            this.token = token;
            return this;
        }

        public Builder setMethod(MethodType method){
            this.methodType = method;
            return this;
        }
        public Builder setTimeout(long timeout){
            this.timeout = timeout;
            return this;
        }

        public Builder setBody(JSONObject body){
            this.body = body;
            return this;
        }

        public Builder setBody(String bodyString) throws JSONException{
            this.body = new JSONObject(bodyString);
            return this;
        }

        public Builder setSSL(boolean ssl){
            this.ssl = ssl;
            return this;
        }
        public Builder setRequsetCallBack(IRequestCallBack callBack){
            this.callBack = callBack;
            return this;
        }

        public Request build() throws IOException{
            Request request = new Request(Url , token , languageType , methodType , timeout , ssl);
            if (methodType != MethodType.GET){
                request.setBody(body);
            }
            request.setCallBack(callBack);
            return request;
        }

    }
}
