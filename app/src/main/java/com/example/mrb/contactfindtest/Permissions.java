package com.example.mrb.contactfindtest;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by mrb on 4/9/18.
 */

public class Permissions {

    Activity activity;
    public Permissions( Activity activity){
        this.activity = activity;
    }

    public boolean checkPermission(String permission){
        boolean check;

        if (ContextCompat.checkSelfPermission(activity.getBaseContext() , permission) != PackageManager.PERMISSION_GRANTED){
            check = false;
        }
        else {
            check = true;
        }

        return check;
    }

    public boolean checkPermissions(String... permissions){
        boolean check= false;
        if (permissions != null){
            for (String permission : permissions){
                if (ContextCompat.checkSelfPermission(activity.getBaseContext() , permission) != PackageManager.PERMISSION_GRANTED){
                    check = false;
                }
                else {
                    check = true;
                }
            }
        }
        return check;
    }

    public void requestPermissions(int requestCode , String... permissions){
        if (!checkPermissions(permissions)){
            ActivityCompat.requestPermissions(activity, permissions, 1);
        }
    }

    public void requestPermission(String permission , int requestCode){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity , permission)  ){

            //TODO make dialog to explain why user should grant this permission
            ActivityCompat.requestPermissions(activity , new String[]{permission} , requestCode);
        }
        else {
            ActivityCompat.requestPermissions(activity , new String[]{permission} , requestCode);
        }
    }

    public  boolean grantingStartUpPermissions(String permission , int requestCode){
        boolean contactsAvailable = checkPermission(permission);
        if (!contactsAvailable){
            requestPermission(permission , requestCode);
            contactsAvailable = checkPermission(permission);
        }

        return contactsAvailable;

    }



}
