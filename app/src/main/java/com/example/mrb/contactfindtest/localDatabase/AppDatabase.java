package com.example.mrb.contactfindtest.localDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {RecentSearch.class} , version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase appDatabase;
    public abstract RecentDao recentDao();

    private static Context context;

    public static AppDatabase getInstance(){
        if (appDatabase == null){
            appDatabase = Room.databaseBuilder(context.getApplicationContext() , AppDatabase.class
            , "recent_database")
                    .allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }

    public static void destroyInstance(){
        appDatabase = null;
    }

}
