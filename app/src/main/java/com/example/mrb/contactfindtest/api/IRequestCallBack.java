package com.example.mrb.contactfindtest.api;

import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by mrb on 4/8/18.
 */

public interface IRequestCallBack {
    void onError(int statusCode);

    void onCancel(boolean canceled);

    void onRetry(int tryCount);

    void onTimeOut();

    void onFinish(@NonNull final JSONObject response);

    NetworkInfo getActiveNetworkInfo();
}
