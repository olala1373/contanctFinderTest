package com.example.mrb.contactfindtest;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.mrb.contactfindtest.activities.SearchResultActivity;
import com.example.mrb.contactfindtest.baseClasses.CallLogBase;
import com.example.mrb.contactfindtest.adapter.CallLogListAdapter;
import com.example.mrb.contactfindtest.api.IRequestCallBack;
import com.example.mrb.contactfindtest.api.Request;
import com.example.mrb.contactfindtest.history.RecentCallLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements  ActivityCompat.OnRequestPermissionsResultCallback
       {

     //<editor-fold desc = "variables">
    private final static int REQUEST_PERMISSION_CONTACT_CODE = 1;
    private final static String CONTACT_PERMISSION = Manifest.permission.READ_CONTACTS;
    private ArrayList<CallLogBase> callLogs;
    private Permissions permissions = new Permissions(this);
    private HashMap<String , String> contacts = new HashMap<>();
    private JsonCreator jsonCreator;
    private RecentCallLog recentCallLog;
    private CallLogListAdapter adapter;
    private final int REQUEST_CALL_LOG_PERMISSION_CODE = 2;
    private RecyclerView recyclerView;
    private int newCount = 0 ;
    private int oldCount = 0 ;
    @BindView(R.id.search_btn)
    ImageButton button;
    @BindView(R.id.search_input)
    EditText editText;
    //</editor-fold>

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        ButterKnife.bind(this);

        getAllContacts();
        initAdapter();

    }

    public void initAdapter(){
        recentCallLog = new RecentCallLog(this);
        recentCallLog.recentCalls();

        callLogs = recentCallLog.getCallLogList();
        oldCount = recentCallLog.getCount();
        recyclerView = findViewById(R.id.recent_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CallLogListAdapter(callLogs , R.layout.call_log_list , this);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @OnClick(R.id.search_btn)
    protected void search(){

        Request search = null;
        String text = editText.getText().toString();
        text = jsonCreator.normalize(text);
        String search_url = getString(R.string.search_url) + text;

        final String TAG = "REQUEST";
        try {
            search = new Request.Builder()
            .setUrl(search_url)
            .setLanguage(Request.LanguageType.EN)
            .setMethod(Request.MethodType.GET)
            .setSSL(false)
            .setRequsetCallBack(new IRequestCallBack() {
                @Override
                public void onError(int statusCode) {
                    Log.v(TAG , statusCode + "error");
                }

                @Override
                public void onCancel(boolean canceled) {
                    Log.v(TAG , canceled + "cancel");
                }

                @Override
                public void onRetry(int tryCount) {
                    Log.v(TAG , "onRetry");
                }

                @Override
                public void onTimeOut() {
                    Log.v(TAG , "time");
                }

                @Override
                public void onFinish(@NonNull JSONObject response) {
                    try {
                        editText.setText("");
                        Intent intent = new Intent(MainActivity.this , SearchResultActivity.class);
                        intent.putExtra("result" , response.toString());
                        startActivity(intent);

                    }
                    catch (NullPointerException e){
                        Log.v(TAG , "null-response");
                        Toast.makeText(getApplicationContext(), "دوباره تلاش کنید" , Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public NetworkInfo getActiveNetworkInfo() {
                    return activeNetworkInfo();
                }
            }).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        search.fetchLoad(this , getString(R.string.progressdiaog_message_search));
    }

    public void getAllContacts(){

        if (permissions.grantingStartUpPermissions(CONTACT_PERMISSION , REQUEST_PERMISSION_CONTACT_CODE)){

            ContentResolver contentResolver = this.getContentResolver();
            Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null , null
                    , null , null);
            if (cursor.moveToFirst()){
                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)))>0){
                        Cursor cur = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null
                                ,ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?" , new String[]{id}, null);
                        while (cur.moveToNext()){
                            String contactNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            contacts.put(contactNumber , name);
                            break;
                        }
                    }
                }while (cursor.moveToNext());
            }
            if (!getSharedPrefenses())
            insertContacts();

            jsonCreator = new JsonCreator(contacts);
        }

    }

    public JSONObject getJson(){

        jsonCreator = new JsonCreator(contacts);
        try {
            return jsonCreator.create();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected NetworkInfo activeNetworkInfo(){
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }

    public void insertContacts(){
        final String TAG = "INSERT";
        if (contacts.size() > 0 ){
            Request insert = null;
            try {
                insert = new Request.Builder()
                        .setUrl(getString(R.string.insert_url))
                        .setSSL(false)
                        .setMethod(Request.MethodType.POST)
                        .setLanguage(Request.LanguageType.EN)
                        .setBody(getJson())
                        .setRequsetCallBack(new IRequestCallBack() {
                            @Override
                            public void onError(int statusCode) {
                                Log.v(TAG , statusCode + "error");
                            }

                            @Override
                            public void onCancel(boolean canceled) {
                                Log.v(TAG , canceled + " cancel");
                            }

                            @Override
                            public void onRetry(int tryCount) {
                                Log.v(TAG , tryCount + " onRetry");
                            }

                            @Override
                            public void onTimeOut() {

                            }

                            @Override
                            public void onFinish(@NonNull JSONObject response) {
                                setSharedPrefrences(true);
                                try {
                                    //Log.v(TAG , String.valueOf(response.toString()));
                                }
                                catch (NullPointerException e){
                                    Toast.makeText(getApplicationContext(), "دوباره تلاش کنید" , Toast.LENGTH_SHORT).show();
                                    Log.v(TAG , "null-response");
                                }
                            }

                            @Override
                            public NetworkInfo getActiveNetworkInfo() {
                                return activeNetworkInfo();
                            }
                        }).build();

                insert.fetchLoad(this , getString(R.string.progressdiaog_message_insert));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    private boolean getSharedPrefenses(){
        SharedPreferences sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(getString(R.string.insert_boolean) , false);
    }

    private void setSharedPrefrences(boolean insert){
        SharedPreferences sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getString(R.string.insert_boolean) , insert);
        editor.commit();
    }

           @Override
           protected void onResume() {
               super.onResume();
               updateRecyclerView();

           }

           public void updateRecyclerView(){
               newCount = recentCallLog.getCount();

               if (recentCallLog.isDataSetChanged(newCount , oldCount)){
                   recentCallLog.recentCalls();
                   callLogs = recentCallLog.getCallLogList();

                   recyclerView.setAdapter(new CallLogListAdapter(recentCallLog.getCallLogList() ,
                           R.layout.call_log_list , this));
                   adapter.notifyDataSetChanged();
                   adapter.setData(callLogs);
                   recyclerView.invalidate();
                   oldCount = newCount;
               }
           }

    @Override
    public void onRequestPermissionsResult(int requestCode ,
                                           @NonNull String[] permissions ,
                                           @NonNull int[] grantResults){

        switch (requestCode){

            case REQUEST_PERMISSION_CONTACT_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getAllContacts();

                }
                else {

                }
                break;
            case REQUEST_CALL_LOG_PERMISSION_CODE:
                Log.v("PERMISSION" , grantResults[0]+"");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.v("PERMISSION" , "granted");
                    recentCallLog.recentCalls();
                }
                break;
        }
    }




}




