package com.example.mrb.contactfindtest.baseClasses;

/**
 * Created by mrb on 4/16/18.
 */

public class Contact {

    private String name ;
    private String phoneNumber;

    public Contact(String name , String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
