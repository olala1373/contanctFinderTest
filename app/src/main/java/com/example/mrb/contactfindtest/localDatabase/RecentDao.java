package com.example.mrb.contactfindtest.localDatabase;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface RecentDao {
    @Query("SELECT * FROM  recentsearch")
    LiveData<List<RecentSearch>> getAll();

    @Insert(onConflict = REPLACE)
    void insert(RecentSearch... recentSearches);

    @Delete
    void delete(RecentSearch recentSearch);

    @Query("Delete from recentsearch")
    void clearAll();

}
