package com.example.mrb.contactfindtest.history;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.mrb.contactfindtest.Permissions;
import com.example.mrb.contactfindtest.baseClasses.CallLogBase;

import java.util.ArrayList;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class RecentCallLog implements ActivityCompat.OnRequestPermissionsResultCallback {

    private String name;
    private String number;
    private int type;
    private int date;
    private int duration;
    private Cursor cursor;
    private int countList;


    private Uri queryUri = android.provider.CallLog.Calls.CONTENT_URI;
    private final String CALL_LOG_PERMISSION = Manifest.permission.READ_CALL_LOG;
    private ArrayList<CallLogBase> callLogList = new ArrayList<>();

    private final int REQUEST_CALL_LOG_PERMISSION_CODE = 2;
    private String[] projection = new String[]{
            ContactsContract.Contacts._ID,
            CallLog.Calls.CACHED_NAME,
            CallLog.Calls.NUMBER,
            CallLog.Calls.TYPE,
            CallLog.Calls.DATE,
            CallLog.Calls.DURATION
    };
    private String sortOrder = String.format("%s limit 50 ", CallLog.Calls.DATE + " DESC");
    Activity activity;
    Permissions permissions;

    public RecentCallLog(Activity activity) {
        this.activity = activity;
        permissions = new Permissions(activity);
        getPermission();
    }



    //<editor-fold desc = "setters and getters">
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    //</editor-fold>

    public boolean getPermission(){
        boolean granted = false;
        if (!permissions.checkPermission(CALL_LOG_PERMISSION)) {
            granted = permissions.grantingStartUpPermissions(CALL_LOG_PERMISSION , REQUEST_CALL_LOG_PERMISSION_CODE);
        }

        return granted;
    }

    public ArrayList<CallLogBase> getCallLogList() {
        return callLogList;
    }

    public void setCallLogList(ArrayList<CallLogBase> callLogList) {
        this.callLogList = callLogList;
    }

    public String getContactName(String phoneNumber){
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName="";
        Cursor cursor = activity.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }



    public void recentCalls() {


        if (!permissions.checkPermission(CALL_LOG_PERMISSION)) {
           permissions.grantingStartUpPermissions(CALL_LOG_PERMISSION , REQUEST_CALL_LOG_PERMISSION_CODE);
        }
        if(permissions.checkPermission(CALL_LOG_PERMISSION)) {
            Cursor cursor = activity.getApplicationContext().getContentResolver().query(queryUri, projection, null, null
                    , sortOrder);
            countList = cursor.getCount();
            callLogList = new ArrayList<>();

            int count = 0;
            String prevName = "";
            CallLogBase callLog = null;
            ArrayList<Integer>  typeList = new ArrayList<>();
            while (cursor.moveToNext()){
                String phone = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                String name = getContactName(phone);
                int id = cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID));
                int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));

                if (phone.equals(prevName)){
                    typeList.add(type);
                    prevName = phone;
                }
                else {
                    if (count > 0){
                        callLog.setType(typeList);
                        callLog.setId(id);
                        callLogList.add(callLog);
                        count = 0;
                        typeList = new ArrayList<>();
                    }

                    typeList.add(type);
                    callLog = new CallLogBase();
                    callLog.setNumber(phone);
                    Log.v("NO NAME" , name+"");
                    if (name.equals("")){

                        callLog.setName(phone);
                    }
                    else
                    callLog.setName(name);
                    prevName = phone;

                    count++;

                }



            }
        }

        setCallLogList(callLogList);

    }

    public boolean isDataSetChanged(int newCount , int oldCount){
        return newCount != oldCount;
    }

    public int getCount(){
        if (permissions.checkPermission(CALL_LOG_PERMISSION)){
            Cursor cursor = activity.getApplicationContext().getContentResolver().query(queryUri, projection, null, null
                    , sortOrder);

            return cursor.getCount();
        }

        return countList;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.v("PERMISSION" , "granted");
        switch (requestCode){
            case REQUEST_CALL_LOG_PERMISSION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.v("PERMISSION" , "granted");
                    recentCalls();
                }
                break;
                default:
                    recentCalls();
        }
    }
}
