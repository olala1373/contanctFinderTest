package com.example.mrb.contactfindtest.baseClasses;

import java.util.ArrayList;

public class CallLogBase {
    private int id;


    private String name;
    private String number;
    private ArrayList<Integer> type;
    private int date;
    private int duration;

    //<editor-fold desc = "getters and setters">
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ArrayList<Integer> getType() {
        return type;
    }

    public void setType(ArrayList<Integer> type) {
        this.type = type;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    //</editor-fold>
}
