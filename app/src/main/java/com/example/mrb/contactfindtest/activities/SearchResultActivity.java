package com.example.mrb.contactfindtest.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mrb.contactfindtest.R;
import com.example.mrb.contactfindtest.baseClasses.Contact;
import com.example.mrb.contactfindtest.adapter.ContactsListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {
    List<Contact> contacts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);


        contacts = parseJson(result());
        if (contacts.size() == 0){
            LinearLayout linearLayout = findViewById(R.id.empty_list);
            linearLayout.setVisibility(View.VISIBLE);
            LinearLayout action = findViewById(R.id.action_section);
            LinearLayout phonePreview = findViewById(R.id.phone_number_preview);
            action.setVisibility(View.GONE);
            phonePreview.setVisibility(View.GONE);
        }
        else {
            TextView textView = findViewById(R.id.phone_number_preview_text);
            textView.setText(contacts.get(0).getPhoneNumber());
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contact_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(new ContactsListAdapter(contacts , R.layout.contacts_card));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    public JSONObject result(){
        JSONObject jsonObject = null;
        try {
             jsonObject = new JSONObject(getIntent().getStringExtra("result"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public List<Contact> parseJson(JSONObject result){
        ArrayList<Contact> contacts = new ArrayList<>();
        try {
            JSONArray list = result.getJSONArray("list");
            for (int i = 0 ; i < list.length() ; i++){
                JSONObject jsonObject = list.getJSONObject(i);

                String name = jsonObject.getString("name");
                String phone = jsonObject.getString("phoneNumber");
                Contact contact = new Contact(name , phone);
                contacts.add(contact);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contacts;
    }
}
