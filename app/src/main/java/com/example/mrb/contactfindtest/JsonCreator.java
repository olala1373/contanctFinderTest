package com.example.mrb.contactfindtest;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by mrb on 4/9/18.
 */

public class JsonCreator {
    HashMap<String , String> contacts;
    public JsonCreator(HashMap contacts){
        Log.v("hhhhh" ,"hiii");
        this.contacts = contacts;
    }
    public JSONObject create() throws JSONException{
        Log.v("string" ,"hiii");
        JSONObject body = new JSONObject();
        JSONArray contactsArray = new JSONArray();
        body.put("list" , contactsArray);
        Set set = contacts.keySet();
        for (Object s : set){
            String phone = s.toString();
            Log.v("string" ,phone);
            phone = normalize(phone);
            String name = contacts.get(s);
            JSONObject array = new JSONObject();
            array.put("name" , name);
            array.put("phoneNumber" , phone);
            contactsArray.put(array);
        }

        return body;
    }


    public String normalize(String phone){
        if (phone.startsWith("0")){
            phone = phone.replaceAll("\\s+","");
            phone = phone.substring(1,phone.length());
            return phone;
        }
        else if (phone.startsWith("+")){
            phone = phone.replaceAll("\\s+","");
            phone = phone.substring(3,phone.length());
            return phone;
        }
        else {
            return phone;
        }
    }
}
